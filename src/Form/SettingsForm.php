<?php

namespace Drupal\popup_block\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;

/**
 * Configure Popup Block settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'popup_block_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['popup_block.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('popup_block.settings');
    $form['background_image_selector'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Background image selector'),
      '#description' => $this->t('CSS class used as selector to apply the background image to the block. <b>IMPORTANT</b> If use a custom value remember apply proper styling in order background to work.'),
      '#default_value' => $config->get('background_image_selector'),
    ];
    $form['load_css'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load CSS from module'),
      '#description' => $this->t("Provides some basic CSS rules to make the module plug-&-play. Uncheck if you're having hard time styling the popup."),
      '#default_value' => $config->get('load_css'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (empty(Html::getClass($form_state->getValue('background_image_selector')))) {
      $form_state->setErrorByName('background_image_selector',
        $this->t('Please, select a valid CSS class'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('popup_block.settings')
      ->set('background_image_selector',
        $form_state->getValue('background_image_selector'))
      ->set('load_css',
        $form_state->getValue('load_css'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
