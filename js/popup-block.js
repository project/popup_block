/**
 * @file
 * Popup Block behaviors.
 */
(function (Drupal, o) {

  'use strict';

  Drupal.behaviors.popupBlock = {
    attach: function (context, settings) {
      for (var blockUuid in settings.popupBlock) {
        o('popup-block', context.querySelector('.' + blockUuid)).forEach(function(element) {
          var button = element.querySelector('button');
          if (button !== null) {
            var body = document.body;
            body.classList.add("popup-block-active");
            button.addEventListener('click', function (event) {
              body.classList.remove("popup-block-active");
              event.target.closest('[popup-block]').classList.add('block-content--hidden')
            });
          }
        });
      }
    }
  };

} (Drupal, once));
